<?php

class Intuit_Challenge_Admin {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function enqueue_styles() {
		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/intuit-challenge-admin.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts() {
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/intuit-challenge-admin.js', array( 'jquery' ), $this->version, false );
	}

	public function customize_challenge_columns($columns)
	{
		$columns['donations'] = __('# Donations', 'intuit-challenge');
		$columns['export'] = __('Export Donations', 'intuit-challenge');
		return $columns;
	}

	public function populate_challenge_columns($column_name, $term_id)
	{
		switch ($column_name) {
			case 'donations':
				$challenge_donations = get_posts([
					'post_type'   => 'int_donation',
					'numberposts' => -1,
					'meta_query' => [
						'relation' => 'AND',
						['key' => 'challenge_id', 'compare' => '=', 'value' => $term_id],
					],
				]);
				echo count($challenge_donations);
				break;
			case 'export':
				echo "<a href='?ic_action=export_donations&challenge_id={$term_id}' class='page-title-action'>".__('Export to CSV', 'intuit-challenge').'</a>';
				break;
		}
	}

	public function admin_challenge_actions() {
		if (!current_user_can('administrator')) {return;}

		if (!empty($_GET['ic_action']) && $_GET['ic_action'] == 'export_donations') {
			$this->export_donations_csv();
		}
	}

	private function export_donations_csv()
	{
		$challenge_id = $_GET['challenge_id'] ?? '';
		if (empty($challenge_id)) {return;}

		$challenge_post = get_post($challenge_id);

		$file_name = "export-" . date('YmdHis') . "-{$challenge_post->post_title}" . '.csv';

		$baseDir = wp_upload_dir()['basedir'] . '/intuit_exports';
		if (!is_dir($baseDir)) {
			mkdir($baseDir, 0755, true);
		}
		$file_path = "$baseDir/$file_name";
		$file = fopen($file_path, "w");
		$bom = chr(0xEF) . chr(0xBB) . chr(0xBF);
		fputs($file, $bom);

		$rows = array([
			'Submit Date',
			'User Name',
			'User Email',
			'Company',
			'Subscribed',
		]);

		$donations = get_posts([
			'post_type'   => 'int_donation',
			'numberposts' => -1,
			'meta_query' => [
				'relation' => 'AND',
				['key' => 'challenge_id', 'compare' => '=', 'value' => $challenge_id],
			],
		]);
		foreach ($donations as $post) {
			$submit_date = date('d/m/y', strtotime($post->post_date));
			$user_id = get_post_meta($post->ID, 'user_id', true);
			$company = get_post_meta($post->ID, 'company', true);
			$user = get_user_by('id', $user_id);
			$user_progress = get_post_meta($challenge_id, 'user_' . $user_id, true);
			$fields = array($submit_date, $user->user_email, $user->display_name, $company, !empty($user_progress['subscribed']));
			$rows[] = $fields;
		}
		$rows_count = count($rows);
		foreach ($rows as $index => $fields) {
			$out = '';

			// UTF-UTF8 separator
			$out = implode(',', $fields);
			$line = $out;
			if ($index + 1 < $rows_count) {
				// UTF-UTF8 new line
				$line .= "\n";
			}
			fputs($file, $line);
		}

		fclose($file);

		header('Content-type: text/csv');
		header("Content-Disposition: attachment; filename=\"$file_name\"");
		readfile($file_path);
		unlink($file_path);
		die;
	}
}
