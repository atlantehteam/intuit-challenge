<?php

class Intuit_Challenge {

	protected $loader;

	protected $plugin_name;

	protected $version;

	public function __construct() {
		if ( defined( 'INTUIT_CHALLENGE_VERSION' ) ) {
			$this->version = INTUIT_CHALLENGE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'intuit-challenge';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_challenge_hooks();
		$this->define_public_hooks();

	}

	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-intuit-challenge-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-intuit-challenge-i18n.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf-fields.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-challenge.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-intuit-challenge-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-intuit-challenge-public.php';

		$this->loader = new Intuit_Challenge_Loader();

	}

	private function set_locale() {

		$plugin_i18n = new Intuit_Challenge_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	private function define_challenge_hooks() {

		$plugin_challenge = new Intuit_Challenge_CPT( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action('init', $plugin_challenge, 'register_cpt');
		$this->loader->add_action('add_meta_boxes', $plugin_challenge, 'remove_yoast');
	}

	private function define_admin_hooks() {

		$plugin_admin = new Intuit_Challenge_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_filter('manage_int_challenge_posts_columns', $plugin_admin, 'customize_challenge_columns');
		$this->loader->add_filter('manage_int_challenge_posts_custom_column', $plugin_admin, 'populate_challenge_columns', 10, 2);
		$this->loader->add_action('admin_init', $plugin_admin, 'admin_challenge_actions', 2);


	}

	private function define_public_hooks() {
		$plugin_public = new Intuit_Challenge_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_filter('single_template', $plugin_public, 'single_template');
		$this->loader->add_action('template_redirect', $plugin_public, 'challenge_actions', 2);
		$this->loader->add_action('init', $plugin_public, 'register_ajax');
	}

	public function run() {
		$this->loader->run();
	}

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_version() {
		return $this->version;
	}

}
