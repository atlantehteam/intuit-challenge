<?php

class Intuit_Challenge_CPT
{
	private $plugin_name;

	private $version;

	public function __construct($plugin_name, $version)
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function register_cpt() {
		$this->register_challenges();
		$this->register_donations();
	}

	private function register_challenges()
	{
		$labels = array(
			'name'                  => _x('Challenges', 'Post Type General Name', 'intuit-challenge'),
			'singular_name'         => _x('Challenge', 'Post Type Singular Name', 'intuit-challenge'),
			'add_new'               => __('New Challenge', 'Post Type Singular Name', 'intuit-challenge'),
			'all_items'               => __('All Challenges', 'Post Type Singular Name', 'intuit-challenge'),
		);
		$args = array(
			'label'                 => __('Challenge', 'intuit-challenge'),
			'labels'                => $labels,
			'supports'              => array('title'),
			'taxonomies'            => array(),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-money-alt',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
            'rewrite' => array('slug' => 'challenges'),
		);
		register_post_type('int_challenge', $args);
	}

	private function register_donations()
	{
		$labels = array(
			'name'                  => _x('Donations', 'Post Type General Name', 'intuit-challenge'),
			'singular_name'         => _x('Donation', 'Post Type Singular Name', 'intuit-challenge'),
			'add_new'               => __('New Donation', 'Post Type Singular Name', 'intuit-challenge'),
			'all_items'               => __('All Donations', 'Post Type Singular Name', 'intuit-challenge'),
		);
		$args = array(
			'label'                 => __('Donation', 'intuit-challenge'),
			'labels'                => $labels,
			'supports'              => array('title'),
			'taxonomies'            => array(),
			'hierarchical'          => false,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => false,
			'has_archive'           => false,
			'exclude_from_search'   => true,
		);
		register_post_type('int_donation', $args);
	}

	public function remove_yoast() {
		remove_meta_box('wpseo_meta', 'int_challenge', 'normal');
	}
}
