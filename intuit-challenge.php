<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              facebook.com/atlanteh
 * @since             1.0.0
 * @package           Woo_Weight_Toggle
 *
 * @wordpress-plugin
 * Plugin Name:       Intuit Challenge
 * Plugin URI:        about:blank
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Roi Nagar
 * Author URI:        facebook.com/atlanteh
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       intuit-challenge
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'INTUIT_CHALLENGE_VERSION', '1.0.0' );
define('INTUIT_CHALLENGE_DIR', plugin_dir_path(__FILE__));
define('INTUIT_CHALLENGE_URI', plugin_dir_url(__FILE__));

function activate_intuit_challenge() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intuit-challenge-activator.php';
	Intuit_Challenge_Activator::activate();
}

function deactivate_intuit_challenge() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intuit-challenge-deactivator.php';
	Intuit_Challenge_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_intuit_challenge' );
register_deactivation_hook( __FILE__, 'deactivate_intuit_challenge' );

require plugin_dir_path( __FILE__ ) . 'includes/class-intuit-challenge.php';

function run_intuit_challenge() {

	$plugin = new Intuit_Challenge();
	$plugin->run();

}
run_intuit_challenge();
