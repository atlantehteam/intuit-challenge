<?php

class Intuit_Challenge_Public {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	public function enqueue_styles() {
		$cache_buster = date("YmdHi", filemtime( plugin_dir_path( __FILE__ ) . 'css/intuit-challenge-public.css'));
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/intuit-challenge-public.css', array(), $cache_buster, 'all' );
	}

	public function enqueue_scripts() {
		$cache_buster = date("YmdHi", filemtime( plugin_dir_path( __FILE__ ) . 'js/intuit-challenge-public.js'));
		wp_enqueue_script( 'glidejs', plugin_dir_url( __FILE__ ) . 'js/glide.js', array( 'jquery' ), '3.5.0', false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/intuit-challenge-public.js', array( 'jquery', 'glidejs' ), $cache_buster, false );
		wp_localize_script($this->plugin_name, 'ITAjax', array(
			'url' => admin_url('admin-ajax.php'),
		));
	}

	public function register_ajax() {
		add_action('wp_ajax_ic_toggle_subscribe', array($this, 'toggle_subscribe'));
	}

	public function single_template($single) {
		global $post;

		/* Checks for single template by post type */
		if ( $post->post_type == 'int_challenge' ) {
			if ( file_exists( INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-template.php' ) ) {
				return INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-template.php';
			}
		}

		return $single;
	}

	private function validate_challenge_active($fields) {
		$now = strtotime('now');
		$challenge_start_time = strtotime($fields['start_time']);
		$challenge_end_time = strtotime($fields['end_time']);

		if ($challenge_start_time > $now || $challenge_end_time < $now) {
			header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
			exit();
		}
	}

	public function toggle_subscribe() {
		$challenge_id = $_POST['challenge_id'];
		$is_subscribed = $_POST['is_subscribed'] == 'true';

		$meta_id = 'user_' . get_current_user_id();
		$current_progress = get_post_meta($challenge_id, $meta_id, true);
		if (!empty($current_progress)) {
			$current_progress['subscribed'] = $is_subscribed;
			update_post_meta($challenge_id, $meta_id, $current_progress);
			wp_send_json(['status' => 'ok']);
		} else {
			wp_send_json(['status' => 'failure']);
		}
	}

	private function start_challenge() {
		// TODO: check if can start
		$fields = get_fields();
		$this->validate_challenge_active($fields);

		$meta_id = 'user_' . get_current_user_id();
		$current_progress = get_post_meta(get_the_ID(), $meta_id, true);
		if ($current_progress) {return;}


		$num_quesions = sizeof($fields['questions']);
		$selected_question_indeces = array_rand(range(0, $num_quesions - 1), $fields['num_questions']);

		$selected_questions = [];
		foreach ($selected_question_indeces as $index) {
			$selected_questions []= $fields['questions'][$index]['id'];
		}
		update_post_meta(get_the_ID(), $meta_id, array('status' => 'started', 'started_at' => date("Y-m-d H:i:s"), 'questions' => $selected_questions));

		header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
		exit();
	}

	private function validate_answers() {
		$fields = get_fields();
		$now = strtotime('now');
		$minutes_to_complete = $fields['minutes_to_complete'];
		$meta_id = 'user_' . get_current_user_id();
		$user_progress = get_post_meta(get_the_ID(), $meta_id, true);
	
		$this->validate_challenge_active($fields);

		$deadline_time = DateTime::createFromFormat("Y-m-d H:i:s", $user_progress['started_at'])->modify("+$minutes_to_complete minutes");

		if (empty($user_progress['status']) || $user_progress['status'] != 'started') {
			header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
			exit();
		}
		
		if ($deadline_time->getTimestamp() < $now) {
			header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
			exit();
		}
		// if ($current_progress) {return;}


		$assigned_questions = $user_progress['questions'];
		$answers = [];
		$new_status = 'passed';
		foreach ($fields['questions'] as $question) {
			if (in_array($question['id'], $assigned_questions)) {
				$answers[$question['id']] = $_POST["question_{$question['id']}"] ?? '__empty__';
				$is_correct = false;
				foreach ($question['answers'] as $answer) {
					if ($answer['is_correct'] && $answer['value'] == $answers[$question['id']]) {
						$is_correct = true;
						break;
					}
				}
				if (!$is_correct) {
					$new_status = 'failed';
				}
			}
		}

		$user_progress['status'] = $new_status;
		$user_progress['answers'] = $answers;
		update_post_meta(get_the_ID(), $meta_id, $user_progress);

		header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
		exit();
	}

	private function select_donation() {
		$fields = get_fields();

		$this->validate_challenge_active($fields);

		$meta_id = 'user_' . get_current_user_id();
		$user_progress = get_post_meta(get_the_ID(), $meta_id, true);
	

		if (empty($user_progress['status']) || $user_progress['status'] != 'passed') {
			header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
			exit();
		}

		$company = $_POST['company'] ?? '__empty__';
		$current_user = wp_get_current_user();
		$post_id = wp_insert_post([
			'post_title' => "{$current_user->display_name} donated to {$company}",
			'post_type' => 'int_donation',
			'post_status' => 'publish',
			'meta_input' => [
				'user_id' => get_current_user_id(),
				'company' => $company,
				'challenge_id' => get_the_ID()
			],
		]);

		$user_progress['status'] = 'completed';
		update_post_meta(get_the_ID(), $meta_id, $user_progress);
		
		$passed_key = 'ic_success_' . date('Y-m-d');
		$passed_today = get_post_meta(get_the_ID(), $passed_key, true) ?: 0;
		update_post_meta(get_the_ID(), $passed_key, $passed_today + 1);

		header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
		exit();
	}

	public function challenge_actions() {
		if (!is_user_logged_in() || empty($_POST['ic_action'])) {
			return;
		}

		switch ($_POST['ic_action']) {
		case 'start_challenge':
			$this->start_challenge();
			break;
		case 'send_answers':
			$this->validate_answers();
			break;
		case 'select_donation':
			$this->select_donation();
			break;
		case 'reset_progress': // TODO: REMOVE
			delete_post_meta(get_the_ID(), 'user_' . get_current_user_id());
			header("Location: {$_SERVER['REQUEST_URI']}", true, 303);
			break;
		}
	}
}
