(function( $ ) {
	'use strict';
$(() => {
	$('.ic_timer[data-end]').each(function() {
		const $timer = $(this);
		const date = new Date($timer.attr('data-end'));
		const handle = setInterval(() => {
			const now = Date.now();
			const millisLeft = Math.max(date.getTime() - now, 0);
			if (millisLeft <= 0) {
				window.location.reload()
			}
			var hours = Math.floor((millisLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((millisLeft % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((millisLeft % (1000 * 60)) / 1000);
			$timer.find('.minutes').html(String(minutes).padStart(2, '0'));
			$timer.find('.seconds').html(String(seconds).padStart(2, '0'));
		}, 1000);
	})

	function selectDonationCmp($form, companyName) {
		$form.find('input[name=company]').val(companyName);
		$form.find('.ic_cmp_content').removeClass('active');
		$form.find('.ic_cmp_btn').removeClass('selected');
		$form.find(`.ic_cmp_content[data-company="${companyName}"]`).addClass('active');
		$form.find(`.ic_cmp_btn[data-company="${companyName}"]`).addClass('selected');
	}
	$(document).on('click mouseenter', '.ic_donation_cmps .ic_cmp_btn', function() {
		const $btn = $(this);
		const $form = $btn.closest('form');
		const companyName = $btn.attr('data-company');
		selectDonationCmp($form, companyName);
	})

	$('.ic-subscribe-toggle input').change(function() {
		const isChecked = $(this).is(':checked');
		const challengeId = $(this).closest('[data-challenge-id]').attr('data-challenge-id');
		const $res = $(this).closest('.ic_subscribe_wrapper').find('.ic-result');
		$res.removeClass('active');
		$.post({
			url: ITAjax.url,
			data: `action=ic_toggle_subscribe&is_subscribed=${isChecked}&challenge_id=${challengeId}`,
		})
		.done(function (res) {
			$res.html(`${isChecked ? 'Subscribed' : 'Unsubscribed'} successfully`);
			$res.addClass('active');
		})
		.fail(function () {
			$res.html(`Failed to ${isChecked ? 'subscribe' : 'unsubscribe'}`);
			$res.addClass('active');
		})
		.always(function () {
			// $form.removeClass('loading');
		})
	})

	if (document.querySelector('.ic_donation_cmps')) {
		const cmps = $('.ic_cmp_btn[data-company]').map(function() {return $(this).attr('data-company')}).toArray();
		const $form = $('.ic_donation_cmps').closest('form');
		const icGlide = new window.Glide('.ic_donation_cmps', {
			perView: 6, type: 'carousel',
			breakpoints: {
				1200: {perView: 3, peek: {before: 100, after: 100}},
				1000: {perView: 2, peek: {before: 100, after: 100}},
				767: {perView: 1, peek: {before: 100, after: 100}},
			}
		}).mount();
		icGlide.on('swipe.end', () => {
			const companyName = cmps[icGlide.index];
			selectDonationCmp($form, companyName);
		})
		window.icGlide = icGlide;
	}

	if ($('.ic_header.sticky').length) {
		$('.elementor[data-elementor-type=header]').addClass('sticky')
	}
})
})( jQuery );
