<?php
get_header();

?>

<?php
$fields = get_fields();
$now = strtotime('now');
$start_time = strtotime($fields['start_time']);
$end_time = strtotime($fields['end_time']);
$minutes_to_complete = $fields['minutes_to_complete'];
$passed_today = get_post_meta(get_the_ID(), 'ic_success_' . date('Y-m-d'), true) ?: 0;
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

// $date = date("Y-m-d H:i:s");
// $date2 = DateTime::createFromFormat("Y-m-d H:i:s", $date);

// if ($num_quesions < $fields['num_questions']) {
//     return;
// }
?>

<?php

if ($start_time > $now) { // event not started
	load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-not-started.php');
} else if ($end_time < $now) { // event finished
	load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-ended.php');
} else if (!is_user_logged_in()) { // not logged in
	load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-login.php');
} else if (empty($user_progress) && $passed_today >= $fields['max_users_per_day']) {  // logged in, not started and max reached
	load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-max-reached.php');
} else if (empty($user_progress)) {  // progress not started
	load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-start.php');
} else { // progress started

	$user_started_at = DateTime::createFromFormat("Y-m-d H:i:s", $user_progress['started_at']);
	$user_should_finish = DateTime::createFromFormat("Y-m-d H:i:s", $user_progress['started_at'])->modify("+$minutes_to_complete minutes");

		if ($user_progress['status'] == 'completed') { // completed challenge
			load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-completed.php');
		} else if ($user_progress['status'] == 'passed') { // passed challenge
			load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-donate.php');
		} else if ($user_should_finish < new DateTime() || $user_progress['status'] == 'failed') { // failed challenge
			load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-failed.php');
    } else { // in progress
			load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-progress.php');
    }
}

echo '<form style="position: fixed; bottom: 130px; right: 10px; z-index: 1000; background: white;" method="post"><input type="hidden" name="ic_action" value="reset_progress" /><button type="submit">reset_progress</button></form>';

?>
<?php
get_footer();