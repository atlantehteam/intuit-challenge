<?php
$fields = get_fields();
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

?>

<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_title spaced">You’ve successfully donated your award!</div>
	</div>
</header>

<main class="ic_content medium completed">
	<div class="ic_text bold center">Giving back to our communities is a cornerstone to who we are.</div>
	<div class="ic_subscribe_wrapper">
		<div class="ic-checkbox ic-subscribe-toggle" data-challenge-id="<?= get_the_ID() ?>">
			<label>
				<input type="checkbox" <?= ($user_progress['subscribed'] ?? false) ? "checked" : "" ?>>
				<span class="ic-check">✓</span>
			</label>
		</div>
		<div class="ic-result"></div>
		<div class="ic_text ic_subscribe_explain">I am interested in receiving more information about Intuit, relevant job opportunities, and more</div>
	</div>
	<hr class="ic_separator"></hr>
	<div class="ic_color_title ic_share_title">Intrigue others and get them excited about doing good</div>
	<div class="ic_text center ic_share_subtitle">
		Linkedin, Facebook, and Instagram are great places to share about your donation as part of the Coding for Good Challenge. Be sure to use our hashtag #IntuitCodingForGoodChallenge
	</div>
	<hr class="ic_separator"></hr>
	<?php load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-social.php'); ?>

</main>