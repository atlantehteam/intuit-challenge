
<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_subtitle spaced">Unfortunately, you didn’t solve the challenge. </div>
		<div class="ic_header_title spaced">Thank you for participating in the Coding for Good Challenge.</div>
	</div>
</header>

<main class="ic_content narrow">
	<div class="ic_color_title">We encourage you to follow us on</div>
	<div class="ic_social_icons">
		<?php
		$fields = get_fields();
		foreach ($fields['social'] as $social) {
			?>
			<a class="ic_social" href="<?= $social['link'] ?>" target="_blank">
				<img class="ic_social_img" src="<?= $social['icon'] ?>" alt="icon" />
				<img class="ic_social_img_hover" src="<?= $social['icon_hover'] ?>" alt="icon hover" />
			</a>
		<?php
		}
		?>
	</div>
	<div class="ic_text center">To remain up to date with future coding challenges</div>
</main>