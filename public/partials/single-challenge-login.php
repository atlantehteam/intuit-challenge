
<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_title spaced">Sign in</div>
	</div>
</header>

<div class="ic_content">
	<div class="ic_login_wrapper">
		<?= do_shortcode('[miniorange_social_login]');?>
	</div>
</div>