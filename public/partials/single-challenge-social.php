<div class="ic_social_icons">
	<a class="ic_social" href="https://www.facebook.com/sharer.php?u=<?= urlencode(get_permalink(get_the_ID())) ?>" target="_blank">
		<img class="ic_social_img" src="<?= INTUIT_CHALLENGE_URI . 'public/images/instagram.svg'?>" alt="icon" />
		<img class="ic_social_img_hover" src="<?= INTUIT_CHALLENGE_URI . 'public/images/instagram-hover.svg'?>" alt="icon hover" />
	</a>
	<a class="ic_social" href="https://www.linkedin.com/shareArticle/?mini=true&title=test&summary=hello&url=<?= get_permalink(get_the_ID()) ?>" target="_blank">
		<img class="ic_social_img" src="<?= INTUIT_CHALLENGE_URI . 'public/images/linkedin.svg'?>" alt="icon" />
		<img class="ic_social_img_hover" src="<?= INTUIT_CHALLENGE_URI . 'public/images/linkedin-hover.svg'?>" alt="icon hover" />
	</a>
	<a class="ic_social" href="https://www.facebook.com/sharer.php?u=<?= urlencode(get_permalink(get_the_ID())) ?>" target="_blank">
		<img class="ic_social_img" src="<?= INTUIT_CHALLENGE_URI . 'public/images/facebook.svg'?>" alt="icon" />
		<img class="ic_social_img_hover" src="<?= INTUIT_CHALLENGE_URI . 'public/images/facebook-hover.svg'?>" alt="icon hover" />
	</a>
</div>