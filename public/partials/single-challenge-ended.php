<?php
$fields = get_fields();
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

?>

<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_title spaced">The Coding for Good Challenge has ended</div>
	</div>
</header>

<main class="ic_content medium ended">
	<div class="ic_text bold center">Over a dozen charities received your prize money. Thank you for using your coding skills for good!</div>
	<div class="ic_color_title ic_share_title">Follow Intuit Israel on</div>
	<?php load_template(INTUIT_CHALLENGE_DIR . 'public/partials/single-challenge-social.php'); ?>
	<div class="ic_text center">To remain up to date with future coding challenges</div>
</main>