<?php
$fields = get_fields();
?>
<header class="ic_header donate">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_subtitle spaced">You’ve completed the challenge!</div>
		<div class="ic_header_title spaced">Now’s the time to donate your award to a charity of your choice.</div>
	</div>
</header>

<main class="ic_content donate">
	<div class="ic_color_title">Select your charity</div>
	<form method="post" class='ic_donations_form'>
		<div class="glide ic_donation_cmps">
			<div class="glide__track" data-glide-el="track">
				<ul class="glide__slides">
				<?php
					foreach ($fields['companies'] as $key => $company) {
						$selected_class = $key == 0 ? 'selected' : '';
						?>
							<button type="button" class="ic_cmp_btn <?=$selected_class?>" data-company="<?= $company['name'] ?>">
								<img class="ic_cmp_img" src="<?= $company['icon'] ?>" />
							</button>
						<?php
					}
				?>
				</ul>
			</div>
		</div>

		<div class="ic_cmp_contents">
			<?php
			foreach ($fields['companies'] as $key => $company) {
				$active_class = $key == 0 ? 'active' : '';
				?>
					<section class="ic_cmp_content <?= $active_class ?>" data-company="<?= $company['name'] ?>">
						<?= $company['content'] ?>
					</section>
				<?php
			}
			?>
		</div>
		<input type="hidden" name="company" value="<?= $fields['companies'][0]['name'] ?>" />
		<input type="hidden" name="ic_action" value="select_donation" />
		<button class="ic_button ic_submit_donation" type="submit">Click to donate your award</button>
	</form>

</main>

