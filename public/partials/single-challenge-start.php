
<?php
$minutes_to_complete = get_field('minutes_to_complete');
$num_questions = get_field('num_questions');
?>
<header class="ic_header sticky">
	<div class="ic_header_timer">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_left">
			<div class="ic_header_title">Coding for good</div>
			<div class="ic_header_title bold">Challenge</div>
		</div>
		<div class='ic_timer'>
			<div class="ic_timer_part_wrapper">
				<div class="ic_timer_part minutes"><?= $minutes_to_complete ?></div>
				<div class="ic_timer_part_text">MINUTES</div>
			</div>
			<div class="ic_timer_separator">:</div>
			<div class="ic_timer_part_wrapper">
				<div class="ic_timer_part seconds">00</div>
				<div class="ic_timer_part_text">SECONDS</div>
			</div>
		</div>
	</div>
</header>

<main class="ic_content start">
	<form method="post" class="ic_start_wrapper">
		<div class="ic_start_instructions">Successfully solve the following <?=$num_questions?> questions within <?= $minutes_to_complete?> minutes</div>
		<button class="ic_button" type="submit">Go!</button>
		<input type="hidden" name="ic_action" value="start_challenge" />
	</form>
</main>