<?php
$fields = get_fields();
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

?>

<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_title spaced">Oops!</div>
	</div>
</header>

<main class="ic_content medium ended">
	<div class="ic_text bold center">
		Lots of folks have been doing good today, which means we have reached our limit of challenge submissions for today.
	</div>
	<div class="ic_color_title ic_share_title">Please try again tomorrow!</div>
</main>