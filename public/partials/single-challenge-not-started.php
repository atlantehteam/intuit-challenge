<?php
$fields = get_fields();
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

?>

<header class="ic_header">
	<div class="ic_header_center">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_title spaced">The Coding for Good Challenge has not started yet</div>
	</div>
</header>
<div class="ic_content"></div>