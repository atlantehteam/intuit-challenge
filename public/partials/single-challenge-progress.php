
<?php
$fields = get_fields();
$now = strtotime('now');
$start_time = strtotime($fields['start_time']);
$end_time = strtotime($fields['end_time']);
$minutes_to_complete = $fields['minutes_to_complete'];
$user_progress = get_post_meta(get_the_ID(), 'user_' . get_current_user_id(), true);

$user_started_at = DateTime::createFromFormat("Y-m-d H:i:s", $user_progress['started_at']);
$user_should_finish = DateTime::createFromFormat("Y-m-d H:i:s", $user_progress['started_at'])->modify("+$minutes_to_complete minutes");

$alphabet = range('A', 'Z');

?>

<header class="ic_header sticky">
	<div class="ic_header_timer">
		<img class="ic_logo" src="<?= INTUIT_CHALLENGE_URI . 'public/images/header-logo.svg'?>" />
		<div class="ic_header_left">
			<div class="ic_header_title">Coding for good</div>
			<div class="ic_header_title bold">Challenge</div>
		</div>
		<div class='ic_timer' data-end='<?=$user_should_finish->format('c')?>'>
			<div class="ic_timer_part_wrapper">
				<div class="ic_timer_part minutes"><?= $fields['minutes_to_complete'] ?></div>
				<div class="ic_timer_part_text">MINUTES</div>
			</div>
			<div class="ic_timer_separator">:</div>
			<div class="ic_timer_part_wrapper">
				<div class="ic_timer_part seconds">00</div>
				<div class="ic_timer_part_text">SECONDS</div>
			</div>
		</div>
	</div>
</header>

<main class="ic_content">
	<form method="post" class='ic_questions_form'>
		<div class='ic_questions_wrapper'>
			<?php
			$user_questions = $user_progress['questions'];
			$current_index = 1;
			foreach ($fields['questions'] as $question) {
				if (in_array($question['id'], $user_questions)) {
					?>
					<div class="ic_question" data-q="<?= $question['id'] ?>">
						<div class="ic_question_num"><?=($current_index++)?></div>
						<div class="ic_question_content">
							<div class="ic_question_title"><?= $question['question'] ?></div>
							<div class="ic_question_answers">
								<?php
									foreach ($question['answers'] as $index => $answer) {
									?>
										<label class='ic_answer'>
											<input type='radio' name='question_<?=$question['id']?>' value='<?=$answer['value']?>' />
											<span class="ic_answer_icon"><?= $alphabet[$index] ?></span>
											<div class='ic_answer_text'><?= $answer['title']?></div>
										</label>
									<?php
									}
								?>
							</div>
						</div>
					</div>
					<?php
				}
			}
			?>
		</div> <!-- ic_questions_wrapper -->
		<input type="hidden" name="ic_action" value="send_answers" />
		<button class="ic_button" type="submit">Submit Answers</button>
	</form>
</main>
